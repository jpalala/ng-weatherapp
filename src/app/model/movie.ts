export class Movie {
  title: String;
  releaseDate: Date;
  cast: Array<Cast> = [];
  poster: string;
  imdbPage: string;

  constructor(title: string, releaseDate:Date, cast: Array<Cast>,  poster: string, imdbPage: string) {
    this.title = title;
    this.releaseDate = releaseDate;
    this.cast = cast;
    this.poster = poster;
    this.imdbPage = imdbPage;

  }
}

export class Cast {
  actor: string;
  role: string;
  constructor(actor: string, role: string) {
    this.actor = actor;
    this.role = role;
   }
}
